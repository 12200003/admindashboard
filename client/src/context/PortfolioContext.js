import { createContext, useReducer } from "react";

export const PortfolioContext = createContext();

export const porfolioReducer = (state,action) => {
    switch (action.type) {
        case 'SET_PROJECT':
            return {
                projects: action.payload,
                projectCount: action.payload.length,
            }
        case 'CREATE_PROJECT':
            return {
                projects: [action.payload, ...state.projects],
                projectCount: state.projectCount + 1
            }
        case 'DELETE_PROJECT':
            return {
                projects: state.projects.filter((p) => p._id !== action.payload._id),
                projectCount: state.projectCount - 1
            }
    }
}

export const PortfolioContextProvider = ({ children }) => {
    const [state,dispatch] = useReducer(porfolioReducer,{
        projects:null,
        projectCount:0
    })

    return (
        <PortfolioContext.Provider value={{...state,dispatch}}>
            {children}
        </PortfolioContext.Provider>
    )
}