import { createContext,useReducer } from "react";

export const CertificateContext = createContext();

export const certificateReducer = (state,action) => {
    console.log("from context ",action.payload)
    switch (action.type) {
        case 'SET_CERTIFICATE':
            return {
                certificates:action.payload,
                certificateCount:action.payload.length,
            }
        case 'CREATE_CERTIFICATE':
            return {
                certificates: [action.payload, ...state.certificates],
                certificateCount: state.certificateCount + 1
            }
        case 'DELETE_CERTIFICATE':
            return {
                certificates:state.certificates.filter((c) => c._id !== action.payload._id),
                certificateCount: state.certificateCount - 1
            }
    }
}

export const CertificateContextProvider = ({children}) => {
    const [state,dispatchCertificate] = useReducer(certificateReducer,{
        certificates:null,
        certificateCount:0
    })
    return (
        <CertificateContext.Provider value={{...state,dispatchCertificate}}>
            {children}
        </CertificateContext.Provider>
    )
}