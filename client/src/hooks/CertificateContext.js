import { CertificateContext } from "../context/CertificateContext";
import { useContext } from "react";

export const useCertificateContext = () => {
    const context = useContext(CertificateContext)

    if (!context) {
        throw Error("Certificate context must be used in the context")
    }
    return context;
}