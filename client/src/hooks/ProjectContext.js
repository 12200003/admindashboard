import { PortfolioContext } from "../context/PortfolioContext";
import { useContext } from "react";

export const usePortfolioContext = () => {
    const context = useContext(PortfolioContext);

    if (!context) {
        throw Error("Portfolio context must be used in side an portfolio context")
    }
    return context;
}