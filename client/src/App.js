import { useState } from "react";
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import Main from "./Main";
import Login from "./components/login/Login";

function App() {
  const [authenticate, setAuthentication] = useState(false);

  const setAuth = (boolean) => {
    setAuthentication(boolean);
  };

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={authenticate?<Navigate to='/home'/>:<Login setAuth={setAuth} />} />
        <Route
          path="home"
          element={
            authenticate ? (
              <Main setAuth={setAuth} />
            ) : (
              <Navigate to='/'/>
            )
          }
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;