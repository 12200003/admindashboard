import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function Login({setAuth}) {

  const navigate = useNavigate();

    const [email,setEmail] = useState('');
    const [password,setPassword] = useState('');
    const [error,setError] = useState('');

    console.log(email)
    console.log(password)

    const handleLogin = (e) => {
      e.preventDefault();
      if (email === '12200003.gcit@rub.edu.bt' && password === 'dalboys66') {
        setAuth(true);
        navigate('/home',{replace:true});
      } else {
        setError("Invalid Email or Password!")
      }
    }

  return (
    <div style={{display:'flex',justifyContent:'center',alignItems:'center'}}>
        <form onSubmit={handleLogin}>
            <h3>Login</h3>
            {error &&
              <div style={{color:'red'}}>
                {error}
              </div>
            }
            <input type='email' name = "email" placeholder='12200003.gcit@rub.edu.bt' required
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <input type='password' name = "password" placeholder='* * * * * * * *' required
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <button className='btn btn-primary'>Login</button>
        </form>
    </div>
  )
}

export default Login