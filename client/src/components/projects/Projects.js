import React from 'react'
import './Projects.css';
import { usePortfolioContext } from '../../hooks/ProjectContext';

function Projects() {

    const {projects,dispatch} = usePortfolioContext();

    const handleDeleteProject = async (id) => {
        const response = await fetch(`http://localhost:5000/api/portfolio/${id}`,{
            method:'DELETE'
        })

        const parseRes = await response.json();

        if (response.ok) {
            dispatch({type:'DELETE_PROJECT', payload:parseRes})
        }
    }

    const handleClick = (id) => {
        const comfirmed = window.confirm("Are you sure you want to delete?")
        if (comfirmed) {
            handleDeleteProject(id)
        }
    }

    return (
        <section style={{marginBottom:50}}>
            <h2 style={{textAlign:'center', marginBottom:40}}>Projects</h2>
            <div className='container projects__container'>
                {projects && projects.length > 0 ? (
                    projects.map((project) => (
                        <article className='project__item' key={project._id}>
                            <div className='project__item-image'>
                                <img
                                    src = "https://firebasestorage.googleapis.com/v0/b/porfolio-a7f65.appspot.com/o/EsJdYSAVoAE2_tr.jpg?alt=media&token=4f361914-a154-45a6-aef9-81490133dfe1"
                                    alt=''
                                />
                            </div>
                            <h3><a href='https://delightful-rabanadas-13d23b.netlify.app/'>{project.title}</a></h3>
                            <button className='btn btn-primary' onClick={() => handleClick(project._id)}>Delete</button>
                        </article>
                    ))
                ):(
                    <></>
                )

                }
            </div>
        </section>
    )
}

export default Projects;