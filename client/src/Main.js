import Header from "./components/header/Header";
import ProjectDetails from "./components/projectDetails/ProjectDetails";
import ProjectForm from "./components/projectForm/ProjectForm";
import Certificates from "./components/certificates/Certificates";
import Projects from './components/projects/Projects';

function Main({setAuth}) {
  return (
    <>
      <Header setAuth = {setAuth}/>
      <ProjectDetails/>
      <ProjectForm/>
      <Certificates/>
      <Projects/>
      
    </>
  );
}

export default Main;