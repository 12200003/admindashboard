const express = require('express')
const router = express.Router();
const {getData, addProject,deleteProject, addCertificate, getCertificate,deleteCertificate} = require('../controllers/portfolioControllers');

router.get('/', getData)

router.post("/",addProject)

router.delete("/:id",deleteProject)

router.post("/certificate",addCertificate)

router.get("/certificate", getCertificate)

router.delete("/certificate/:id",deleteCertificate)

module.exports = router;