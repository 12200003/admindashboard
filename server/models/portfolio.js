const mongoose = require('mongoose');

const Schema = mongoose.mongoose.Schema;

const projectSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    gitLink: {
        type: String,
        required: true
    }
}, {timestamps: true})

module.exports = mongoose.model("project", projectSchema)