const Project = require('../models/portfolio');
const Certificate = require('../models/certificate');
const mongoose = require('mongoose');

const getData = async (req,res) => {
    const projects = await Project.find({}).sort({createdAt:-1});
    res.status(200).json(projects);
}

const getCertificate = async (req,res) => {
    const certificate = await Certificate.find({}).sort({createdAt:-1});
    res.status(200).json(certificate);
}

const addProject = async (req,res) => {
    const {title,image,gitLink,url} = req.body;

    try {
        const project = await Project.create({
            title,
            url,
            image,
            gitLink
        })
        res.status(200).json(project);
    } catch (error) {
        res.status(400).json({error:error.message});
    }
}

const addCertificate = async (req,res) => {
    const {title,image} = req.body;
    try {
        const certificate = await Certificate.create({
            title,
            image
        })
        res.status(200).json(certificate);
    } catch (error) {
        res.status(400).json({error:error.message});
    }
}

const deleteProject = async (req,res) => {
    const {id} = req.params;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(404).json({error:"No such project"})
    }

    const project = await Project.findOneAndDelete({_id:id});
    if (!project) {
        return res.status(404).json({error:"No such project"})
    }
    res.status(200).json(project);
}

const deleteCertificate = async (req,res) => {
    const {id} = req.params;
    console.log("calling ",id)

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(404).json({error:"No such certificate"})
    }

    const certificate = await Certificate.findOneAndDelete({_id:id});
    if (!certificate) {
        return res.status(404).json({error:"No such project"})
    }
    res.status(200).json(certificate);
}

module.exports = {
    getData,
    addProject,
    deleteProject,
    addCertificate,
    getCertificate,
    deleteCertificate
}